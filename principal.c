//Octubre 27 de 2015
//Mario Samudio, Audrey Monsalve  && Camilo Cardenas
//Programa para controlar el ingreso por contraseñas.
//Version 3.0

#include <18F4550.h>
#fuses   HSPLL,NOWDT,NOPROTECT,NOLVP,NODEBUG,PUT,USBDIV,PLL5,CPUDIV1,NOMCLR,VREGEN
#use  delay(clock=4000000)//se debe utilizar un cristal de 4Mhz

/*Definiciones para: Pulsadores, leds de confirmación 
   y 4 más  para cada digito de la clave.             */
#define  SW01  PIN_D0
#define  SW02  PIN_D1

#define  LedRojo  pin_C7 // Led rojo que significa error
#define  LedVerde  pin_C6 // Led verde que significa éxito

#define  LED01 pin_D7
#define  LED02 pin_D6
#define  LED03 pin_D5
#define  LED04 pin_D4

#define LCD_E     PIN_C2
#define LCD_CK    PIN_D0   
#define LCD_DAT   PIN_D1

#define buzzer PIN_E2

#include <stdlib.h>
#include <KBD18F.c>           // Teclado matricial
#include <24512.c>            // memoria eeprom
#include <LCD4x20_3PIN.c>     // LCD
#include <TONES.C>

int  datoe[6], datoi[2], a, vacio[2]={0,0};
char  datom = '\0';
INT OPCION[1];
int   clave[4]={0,0,0,0}, i=0, j=0, k=0, contador=0;
//char  datom = '\0', datoeprom[6]= {'\0','\0','\0','\0','\0','\0'}, dato[4]= {'\0','\0','\0','\0'};

// configuraciones
void config_pic()
{
   //limpiar registros, probar limpiar todos los registros en assembler con =0
   //limpiarRegistros();
   #zero_ram
   // Declaración de pines de puertos
   set_tris_A(0x00);/* (0000 0000)
      RA0   -----   PIN   02
      RA1   -----   PIN   03
      RA2   -----   PIN   04
      RA3   -----   PIN   05
      RA4   -----   PIN   06
      RA5   -----   PIN   07
   */

   // Configuración de pines para el teclado
   set_tris_B(0x0F);/* (0000 1111)
      RB0   COL0    PIN   40
      RB1   COL1    PIN   39
      RB2   COL2    PIN   38
      RB3   COL3    PIN   37
      RB4   Fila0   PIN   36
      RB5   Fila1   PIN   35
      RB6   Fila2   PIN   34
      RB7   Fila3   PIN   33
   */

   set_tris_C(0x00);/* (0000 0xxx)
      
      RC0   SCL     PIN   15
      RC1   SDA     PIN   16   
      RC2   LCD_E   PIN   17
      RC6   LED01   PIN   25
      RC7   LED02   PIN   26
   */

   set_tris_D(0x00);/* (0000 xx00)
      RD0   LCD_DAT PIN   19 
      RD1   LCD_CK  PIN   20
      RD2   SW01    PIN   21
      RD3   SW02    PIN   22
      RD4   LED03   PIN   27
      RD5   LED04   PIN   28
      RD6   LED05   PIN   29
      RD7   LED06   PIN   30
   */

   set_tris_E(0x00);/* bits(-0-- xxxx)
      RE0   -----   PIN   08 
      RE1   -----   PIN   09
      RE2   buzzer  PIN   10
      RE3   -----   PIN   01
   */

   //Habilitando y deshabilitando modulos...
   setup_timer_1(T1_disabled);
   setup_timer_2(T2_disabled,0,1);
   setup_timer_3(T3_disabled);
   setup_ccp1(CCP_OFF);
   setup_ADC(ADC_OFF);
   setup_VREF(FALSE);//no voltaje de referencia
   setup_COMPARATOR(NO_ANALOGS);//
}
//metodos led
void apagaleds()
{
   output_low(LED01);
   output_low(LED02);
   output_low(LED03);
   output_low(LED04);
}
// teclado
void teclado(int op)
{
   if(op == 1)
      LCD_PUTC("\f DIGITE SU Clave");
   else
      lcd_putc("\f NUEVA Clave");
   // Contrasena definida en: *8BC
   //    clave[0] = 0x2a;// ===> *
   //    clave[1] = 0x38;// ===> 8
   //    clave[2] = 0x42;// ===> B
   //    clave[3] = 0x43;// ===> C

   // Ciclo que determina el ingreso de solo 4 digitos.
   for (i = 0; i < 4; i++)
   {
      // Condicionando el resultado de lectura para cada digito de la contraseña.
      switch(i+1)
      {

         // Para el caso del digito 1
         case 1:

            // Ciclo ajustado en 6 veces a la espera, lectura y asignación del digito.
            for (j = 0; j < 6; j++)
            {

               // Ciclo ajustado para obtener 1000 iteraciones en aproximación a 1 segundo.
               for (k = 0; k < 100; k++)
               {
                  datom = kbd_getc();
                  delay_ms(10);
                  if (datom != '\0')
                  {
                     generate_tone(C_NOTE[0],100);
                     clave[i] = datom - 0x30; //quitando el ASCi clave[0] = 0x3i -0x30 = 0x0i
                     j = 6;
                     k = 100;
                     lcd_gotoxy(6,2);
                     printf(lcd_putc,"*");
                  }
               }
            }

            // Si no hay inserción del digito entonces se asigna un numeral.
            if (datom == '\0')
            {
               output_high(LedRojo);
               output_high(LED01);
               delay_ms(2000);
               clave[i] = '#';
               output_low(LedRojo);
               lcd_gotoxy(6,2);
               printf(lcd_putc,"#");
            }else
               output_high(LED01);
         break;

         // Para el caso del digito 2
         case 2:

            // Condicional que verifica si aún se mantiene presionada la tecla anterior
            while(datom == (clave[i-1] + 0x30))// poniendo el ASCi para comprobar
            {
               datom = kbd_getc();

               // Alerta roja si mantiene la tecla presionada demasiado tiempo
               if (contador == 100)
               {
                  output_low(LED01);
                  output_high(LedRojo); //
                  contador = 0;   
               }
               contador++;
            }

            output_high(LED01);
            output_low(LedRojo);
            contador = 0;

            for (j = 0; j < 6; j++)
            {
               for (k = 0; k < 100; k++)
               {
                  datom = kbd_getc();
                  delay_ms(10);
                  if (datom != '\0')
                  {
                     generate_tone(E_NOTE[0],100);
                     swap(clave[i-1]);// Cambio de niveles
                     clave[i]= datom-0x30; // le quito el asci
                     clave[i-1] += clave[i];                  
                     j = 6;
                     k = 100;
                     lcd_gotoxy(7,2);
                     printf(lcd_putc,"*");
                  }
               }
            }

            if (datom == '\0')
            {
               output_high(LedRojo);
               output_high(LED02);
               delay_ms(2000);
               clave[i] = '#';
               output_low(LedRojo);
               lcd_gotoxy(7,2);
                     printf(lcd_putc,"#");
            }else
               output_high(LED02);
         break;

         // Para el caso del digito 3
         case 3:

            while(datom == (clave[i-1] + 0x30))
            {
               datom = kbd_getc();
               if (contador == 100)
               {
                  output_low(LED02);
                  output_low(LED01);
                  output_high(LedRojo);
                  contador = 0;   
               }

               contador++;
            }

            output_high(LED02);
            output_high(LED01);
            output_low(LedRojo);
            contador = 0;

            for (j = 0; j < 6; j++)
            {
               for (k = 0; k < 100; k++)
               {
                  datom = kbd_getc();
                  delay_ms(10);
                  if (datom != '\0')
                  {
                     generate_tone(G_NOTE[0],100);
                     clave[i] = datom -0x30;
                     j = 6;
                     k = 100;
                     lcd_gotoxy(8,2);
                     printf(lcd_putc,"*");
                  }
               }
            }

            if (datom == '\0')
            {
               output_high(LedRojo);
               output_high(LED03);
               delay_ms(2000);
               clave[i] = '#';
               output_low(LedRojo);
               lcd_gotoxy(8,2);
               printf(lcd_putc,"#");
            }else
               output_high(LED03);
         break;

         // Para el caso del digito 4
         case 4:

            while(datom == (clave[i-1] + 0x30))
            {
               datom = kbd_getc();
               if (contador == 100)
               {
                  output_low(LED01);
                  output_low(LED02);
                  output_low(LED03);
                  output_high(LedRojo);
                  contador = 0;   
               }

               contador++;
            }

            output_high(LED03);
            output_high(LED02);
            output_high(LED01);
            output_low(LedRojo);
            contador = 0;

            for (j = 0; j < 6; j++)
            {
               for (k = 0; k < 100; k++)
               {
                  datom = kbd_getc();
                  delay_ms(10);
                  if (datom != '\0')
                  {
                     generate_tone(B_NOTE[0],100);
                     swap(clave[i-1]);
                     clave[i] = datom -0x30;
                     clave[i-1] += clave[i];
                     j = 6;
                     k = 100;
                     lcd_gotoxy(9,2);
                     printf(lcd_putc,"*");
                  }
               }
            }
            if (datom == '\0')
            {
               output_high(LedRojo);
               output_high(LED04);
               delay_ms(2000);
               clave[i] = '#';
               output_low(LedRojo);
               lcd_gotoxy(9,2);
               printf(lcd_putc,"#");
            }else
               output_high(LED04);

            // Ultima comprobación
            while(datom == (clave[i-1] + 0x30))
            {
               datom = kbd_getc();
               if (contador == 100)
               {
                  output_low(LED02);
                  output_low(LED01);
                  output_high(LedRojo);
                  contador = 0;   
               }

               contador++;
            }

            output_high(LED02);
            output_high(LED01);
            output_low(LedRojo);
            contador = 0;
         break;
      }
   }
}

void opcion_menu()
{
   boolean valido = false;
   apagaleds();
   lcd_putc("\f DIGITE SU OPCION:");
   delay_ms(2500);
   lcd_putc("\f");
   delay_ms(50);
   do
   {
      DATOM = KBD_GETC();
      if(DATOM != '\0')
      {
         opcion[0] = DATOM - 0X30;
         SWAP(opcion[0]);
         delay_ms(50);
         valido = true;
      }
    delay_ms(50);
    }
    while(valido == false);
    
   }

//Memorias
void escribirMemoriaExterna(int a)
{
   teclado(2);
   apagaleds();
   switch(a)
   {
      case 1:
         write_ext_eeprom(0,CLAVE[0]);
         delay_ms(5);
         write_ext_eeprom(1,CLAVE[1]);
         break;
      case 2:
         write_ext_eeprom(2,CLAVE[0]);
         delay_ms(5);
         write_ext_eeprom(3,CLAVE[1]);
         break;
      case 3:
         write_ext_eeprom(4,CLAVE[0]);
         delay_ms(5);
         write_ext_eeprom(5,CLAVE[1]);
         break;
   }
}

void leerMemoriaExterna()
{
   for(i=0; i<6; i++)
   {
      datoe[i] = read_ext_eeprom(i);
   }

   // contraseña administrador en memoria interna
   datoi[0] =  read_eeprom(0);
   datoi[1] =  read_eeprom(1);
}


//Menus
void menu_mario()
{
   lcd_putc("\f");
   lcd_putc("   Bienvenido   ");
   lcd_putc("\n");
   lcd_putc("     Mario     ");
   delay_ms(2500);
   menu_mario:
   lcd_putc("\f");
   lcd_putc("MENU: \n1. CAMBIAR CLAVE");
   delay_ms(2500);
   
   opcion_menu();
   
   if(opcion[0] == 16)
   {
      delay_ms(50);
      generate_tone(F_NOTE[2],100);
      escribirMemoriaExterna(1);
   }
   else
   {
      printf(LCD_PUTC,"\fERROR DE OPCION");
      delay_ms(2500);
      goto menu_mario;
   }
   
}

void menu_audrey()
{
   lcd_putc("\f");
   lcd_putc("   Bienvenida   ");
   lcd_putc("\n");
   lcd_putc("     Audrey     ");
   delay_ms(2500);
   menu_audrey:
   lcd_putc("\fMENU: \n1. CAMBIAR CLAVE");
   delay_ms(2500);
   
   opcion_menu();
   
   if(opcion[0] == 16)
   {
      delay_ms(50);
      generate_tone(F_NOTE[2],100);
      escribirMemoriaExterna(2);
   }
   else
   {
      printf(LCD_PUTC,"\fERROR DE OPCION");
      delay_ms(2500);
      goto menu_audrey;
   }
}

void menu_camilo()
{
   lcd_putc("\f");
   lcd_putc("   Bienvenido   ");
   lcd_putc("\n");
   lcd_putc("     Camilo     ");
   delay_ms(2500);
   menu_camilo:
   lcd_putc("\fMENU: \n1. CAMBIAR CLAVE");
   delay_ms(2500);
   
   opcion_menu();
   
   if(opcion[0] == 16)
   {
      delay_ms(50);
      generate_tone(F_NOTE[2],100);
      escribirMemoriaExterna(3);
   }
   else
   {
      printf(LCD_PUTC,"\fERROR DE OPCION");
      delay_ms(2500);
      goto menu_camilo;
   }
}

void menu_general()
{
   lcd_putc("\f");
   LCD_PUTC(" Bienvenidos al");
   lcd_putc("\n");
   lcd_putc("    Grupo 2  ");
   apagaleds();
   delay_ms(2500);
   menu:
   lcd_putc("\f");
   lcd_putc("Menu: 1. Mario");
   delay_ms(2500);
   lcd_putc("\n");
   lcd_putc("2. Audrey");
   delay_ms(2500);
   lcd_putc("\f");
   lcd_putc("1. Mario 2. Audrey");
   lcd_putc("\n");
   lcd_putc("    3. Camilo   ");
   delay_ms(2500);
   
   opcion_menu();
   
   if(opcion[0] == 16)
   {
      delay_ms(50);
      generate_tone(F_NOTE[2],100);
      escribirMemoriaExterna(1);
   }  
   else
   {
      if(opcion[0] == 32)
      {
         delay_ms(50);
         generate_tone(Ab_NOTE[2],100);      
         menu_audrey();
      }
      else
      {
         if(opcion[0] == 48)
         {
            delay_ms(50);
            generate_tone(B_NOTE[2],100);      
            menu_camilo();
         }
         else
         {
            printf(LCD_PUTC,"\fERROR");
            for (i = 0; i < 255; i++)
            {
               output_high(buzzer);
               delay_us(i);
               output_low(buzzer);
               delay_us(i);
            }   
            delay_ms(2500);
            goto menu;
         }
      }
   }
   
}

void presentacion()
{
   LCD_PUTC("\f ");
   LCD_PUTC("\f  Bienvenidos\n       a");
   delay_ms(3000);
   lcd_putc("\f   Teoria de\n    Control");
   delay_ms(3000);
   LCD_PUTC("\fMario Samudio");
   LCD_PUTC("\nAudrey Monsalve");
   delay_ms(3000);
   LCD_PUTC("\fCamilo Cardenas");
   delay_ms(3000);
}


//Música 
void musicaerror()
{

   generate_tone(C_NOTE[1], 250);
   generate_tone(Bb_NOTE[0], 250);

   generate_tone(A_NOTE[0], 250);
   generate_tone(Bb_NOTE[1], 250);

   generate_tone(A_NOTE[1], 250);
   generate_tone(C_NOTE[2], 250);

   delay_ms(1250);
}

void fin()
{
   for (i = 0; i < 2; i++)
   {

      generate_tone(D_NOTE[1], 250);
      generate_tone(Eb_NOTE[2], 250);
      
      generate_tone(F_NOTE[3], 250);
      generate_tone(D_NOTE[2], 250);

      generate_tone(F_NOTE[2], 250);
      generate_tone(Eb_NOTE[1], 250);

      delay_ms(1250);
   }
}

//Principal
void main()
{
   config_pic();
      //limpie los puertos
   OUTPUT_A(0x00);
   OUTPUT_B(0x00);
   OUTPUT_C(0X00);
   OUTPUT_D(0X00);
   output_low(buzzer);

   lcd_init();
   KBD_INIT();
   init_ext_eeprom();
   presentacion();

   
   // Adminis = 1234
      // contraseña administrador
   write_eeprom(0,0x12);
   write_eeprom(1,0x34);

   // for (i = 0; i < 6; i++)
   // {
   //    write_ext_eeprom(i, vacio[0]);
   //    delay_ms(5);
   //    write_ext_eeprom(i, vacio[1]);
   //    delay_ms(5);
   // }
   // camilo = 0000
   // audrey = 0000
   // mario  = 0000
   
   inicio:
   leerMemoriaExterna(); // Leer las claves
   
   teclado(1); // Digitar la clave
   
   if(CLAVE[0] == datoi[0] && datoi[1] ==  0x34)
      menu_general();
   else
   {
    
      if(CLAVE[0] == datoe[0] && CLAVE[1] == datoe[1])
      {
         output_low(LED01);
         output_low(LED02);
         output_low(LED03);
         output_low(LED04);
         lcd_putc("\f");
         lcd_gotoxy(2,1);
         lcd_putc("clave correcta.");

         // Confirmación de exito en intervalo progresivo
         for (i = 0; i < 10; ++i)
         {
            output_low(LedVerde);
            delay_ms(i);
            output_high(LedVerde);
            delay_ms(i);
         }

         output_low(LedVerde);
         lcd_putc("\f");
         menu_mario();
      }

      else if(CLAVE[0] == datoe[2] && CLAVE[1] == datoe[3])
      {
         output_low(LED01);
         output_low(LED02);
         output_low(LED03);
         output_low(LED04);
         lcd_putc("\f");
         lcd_gotoxy(2,1);
         lcd_putc("Clave Correcta.");

         // Confirmación de exito en intervalo progresivo
         for (i = 0; i < 10; ++i)
         {
            output_low(LedVerde);
            delay_ms(i);
            output_high(LedVerde);
            delay_ms(i);
         }

         output_low(LedVerde);
         lcd_putc("\f");
         menu_audrey();
      }
      else if(CLAVE[0] == datoe[4] && CLAVE[1] == datoe[5])
      {
         output_low(LED01);
         output_low(LED02);
         output_low(LED03);
         output_low(LED04);
         lcd_putc("\f");
         lcd_gotoxy(2,1);
         lcd_putc("clave Correcta.");

         // Confirmación de exito en intervalo progresivo
         for (i = 0; i < 10; ++i)
         {
            output_low(LedVerde);
            delay_ms(i);
            output_high(LedVerde);
            delay_ms(i);
         }

         output_low(LedVerde);
         lcd_putc("\f");
         menu_camilo();
      }
      else
      {
         output_low(LED01);
         output_low(LED02);
         output_low(LED03);
         output_low(LED04);
         lcd_putc("\f");
         lcd_gotoxy(3,1);
         lcd_putc("Clave errada.");
         delay_ms(1000);
         musicaerror();

         // Confirmación del error en intervalo igual
         for ( i = 0; i < 3; ++i)
         {
            output_low(LedRojo);
            delay_ms(250);
            output_high(LedRojo);
            delay_ms(50);
         }

         delay_ms(2500);
         output_low(LedRojo);
         lcd_putc("\f");
         goto inicio;
      }
   }
   
   LCD_PUTC("\f Terminado.");
   delay_ms(200);
   fin();

   // Variable contraseña debe quedar vacia para un nuevo intento
   clave=0;

   //vuelva a inicio y vuelva y arranque
   reset_cpu();
}